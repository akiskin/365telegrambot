const cfg = require('./config');

const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(cfg.TOKEN);

var axios = require('axios');

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());

var MongoClient = require('mongodb').MongoClient;
MongoClient.connect(cfg.mongourl).then(function(db) {
    app.locals.db = db;
    console.log("Connected successfully to DB");
}).catch(function(err) {
    console.log(`Not connected to DB: ${err}`);
    process.exit(1);
});


//Test if App works
app.get(`/`, (req, res) => {
    res.sendStatus(200);
});

// We are receiving updates from Telegram at the route below!
//The only "public" route (available from outside of perimeter)
app.post(`/${cfg.publicurlprefix}`, (req, res) => {
    bot.processUpdate(req.body);
    res.sendStatus(200);
});




//Store server url & credentials
app.post(`/dataserver`, (req, res) => {

    if (req.body.url == undefined) {
        res.status(400).send("URL missing");
        return
    }
    if (req.body.login == undefined) {
        res.status(400).send("LOGIN missing");
        return
    }
    if (req.body.password == undefined) {
        res.status(400).send("PASSWORD missing");
        return
    }

    newparams = {
        url: req.body.url,
        login: req.body.login,
        password: req.body.password
    }
    query = {
        url: req.body.url
    }

    req.app.locals.db.collection('dataservers')
        .update(query, newparams, {upsert: true}).then(function(result) {

        res.status(200).json(newparams);

    }).catch(function(err){
        res.status(500).send(err);
    });

});



//Get start phrases for region
app.get(`/startphrase`, (req, res) => {

    if (req.query.region == undefined) {
        res.status(400).send("REGION missing");
        return
    }

    var region = parseInt(req.query.region);
    if (isNaN(region)) {
        res.status(400).send("REGION is not a number");
        return
    }

    query = {
        region: region
    }

    req.app.locals.db.collection('startphrases')
        .find(query).toArray().then(function(result) {

        result = result.map( value => {
            value.url = `${cfg.BOTURL}?start=${value.phrase}`;
            return value;
        });

        res.status(200).json(result);

    }).catch(function(err){
        res.status(500).send(err);
    });

});

//Store start phrase
app.post(`/startphrase`, (req, res) => {

    if (req.body.phrase == undefined) {
        res.status(400).send("PHRASE missing");
        return
    }
    if (req.body.region == undefined) {
        res.status(400).send("REGION missing");
        return
    }
    if (req.body.server == undefined) {
        res.status(400).send("SERVER missing");
        return
    }

    newparams = {
        phrase: req.body.phrase,
        region: req.body.region,
        server: req.body.server,
        date: new Date()
    }
    query = {
        phrase: req.body.phrase
    }

    req.app.locals.db.collection('startphrases')
        .update(query, newparams, {upsert: true}).then(function(result) {

        newparams.url = `${cfg.BOTURL}?start=${newparams.phrase}`;
        res.status(200).json(newparams);

    }).catch(function(err){
        res.status(500).send(err);
    });

});


app.post(`/bindchat`, (req, res) => {
    if (req.body.phrase == undefined) {
        res.status(400).send("PHRASE missing");
        return
    }
    if (req.body.chatid == undefined) {
        res.status(400).send("CHATID missing");
        return
    }


    bindChat(req.body.phrase, req.body.chatid).then( result => {

        if (result) {
            res.sendStatus(200);
        } else {
            res.status(400).send("PHRASE not found");
        }

    }).catch( err => {

       res.status(400).send(err); 
    })




});



function bindChat(startphrase, chatid) {

    return new Promise(function(resolve, reject) {

        app.locals.db.collection('startphrases')
            .findOne({phrase: startphrase})
        .then( result => {
            if (result == null) {
                console.log('no startphrase data found');
                throw cfg.dict.StartphraseNotFound
            };
            console.log(`Found region: ${result.region}`);
            return {region: result.region, server: result.server};

        })
        .then( data => {
            query = {chatid: chatid};
            newparams = {
                chatid: chatid,
                region: data.region,
                server: data.server,
                phrase: startphrase
            };

            //return app.locals.db.collection('chats').insertOne(newparams)
            return app.locals.db.collection('chats').update(query, newparams, {upsert: true})
        })
        .then( result => {
            resolve( true );
        })
        .catch( err => {
            console.log(err);
            reject(err);
        });

    })

}

// Start Express Server
app.listen(cfg.port, cfg.localip, () => {
    console.log(`Express server is listening on ${cfg.port}`);
});

//local debugging without valid SSL
if (cfg.uselocaltunnel) {
    var localtunnel = require('localtunnel');
    var tunnel = localtunnel(cfg.port, function(err, tunnel) {
        //const incomingurl = `${tunnel.url}:${cfg.port}/${cfg.publicurlprefix}`;
        const incomingurl = `${tunnel.url}/${cfg.publicurlprefix}`;
        console.log(incomingurl);
        bot.setWebHook(incomingurl);
    });
} else {
    bot.setWebHook(`${cfg.url}`);
}


//BOT RESPONSES
bot.on('message', msg => {
    if (msg.chat.type != 'private') {return} //не "прямые" чаты не обрабатываем
    if (/\/start (.+)/.test(msg.text)) {return} //start command supported separately

    var region;

    app.locals.db.collection('chats').findOne({chatid: msg.chat.id})
    .then( chatconnection => {
        if (chatconnection == null) {throw 'Чат не связан с базой'}

        region = chatconnection.region;
        console.log(`Чат связан с ${region}, ищем сервер ${chatconnection.server}`);

        return app.locals.db.collection('dataservers').findOne({url: chatconnection.server})
    }).then( serverparams => {
        //return exec1c(serverparams, msg.text)
        if (serverparams == null) {throw `Не найдены параметры сервера`}

        console.log(`Сервер ${serverparams.url}`);

        targeturl = serverparams.url + '/hs/365internalapi/telegrammessage';
        body = {
            region: region,
            chatid: msg.chat.id,
            message: msg
        };
        config ={
            auth: {
                username: serverparams.login,
                password: serverparams.password
            }
        };
        return axios.post(targeturl, body, config)
    })
    .then( response => {   
        if (response.status == 200) {
            //bot.sendMessage(msg.chat.id, `Ответ от 1С: ${JSON.stringify(response.data)}`);
            bot.sendMessage(msg.chat.id, response.data.text, response.data.options);
        } else {
            bot.sendMessage(msg.chat.id, `База не доступна: ${response}`);
        }

    }).catch( err => {
        bot.sendMessage(msg.chat.id, `Ошибка: ${err}`);
    })

});

bot.onText(/\/start (.+)/, (msg, match) => {

    const chatid = msg.chat.id;
    const startphrase = match[1]; // the captured "whatever"

    console.log(`Received startphrase: ${startphrase}`);
    bindChat(startphrase, chatid).then( result => {

        if (result) {
            bot.sendMessage(chatid, cfg.dict.ChatBound);
            bot.sendMessage(chatid, cfg.dict.Beginning);
        } else {
            bot.sendMessage(chatid, cfg.dict.StartphraseNotFound);
        }

    }).catch( err => {
        bot.sendMessage(chatid, `Ошибка: ${err}`); 
    })

});

