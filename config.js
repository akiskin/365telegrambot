var config = {};

config.uselocaltunnel = true; //for local debug without valid SSL

config.localip = '0.0.0.0';
config.mongourl = 'mongodb://localhost:27017/365telegrambot';

//Telegram
config.TOKEN = '359115519:AAHlIbVq5h40xbqfjWlWe9dOi2P6YP772uk';
config.BOTURL = 'https://telegram.me/Test365bot';

config.publicurlprefix = "bot";
config.url = `https://test.bit-live.ru/${config.publicurlprefix}`; //This is to be exposed in NGINX
config.port = 88;

//localization
config.dict = {
    ChatBound: 'Отличное начало. Все работает',
    StartphraseNotFound: 'Стартовая фраза не найдена, либо устарела. Создайте новую в настройках базы',
    Beginning: 'Начните с команды: Отчеты - увидите полный список доступных команд'
};

module.exports = config;